let url = document.URL;
if (url.includes("constructor")) {
    let date = new Date();
    let group = document.createElement("button");
    group.class = 'group';
    group.innerHTML = '10703217';
    group.addEventListener("click", changeTimeTable);

    let content = document.querySelector(".content");
    content.appendChild(group);

    function changeTimeTable() {
        let daysArrRu = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
        let daysArrEng = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        let timetableDays = document.createElement('div');
        timetableDays.className = 'timeTablesDays';

        for (let i in daysArrRu) {
            let day =  document.createElement('div');
            day.className = daysArrEng[i];
            day.style.margin = "30px";
            let spanDays = document.createElement('span');
            spanDays.innerHTML = daysArrRu[i];
            let table = document.createElement("select");
            table.onchange = function () {
                createLessons(table.options[table.selectedIndex].value, day);
                
                if (!document.querySelector('.submitBtn')) submitTimeTable();
            };

            for (let i = 0; i < 5; i++) {
                let option = document.createElement('option');
                option.value = String(i + 1);
                option.innerHTML = String(i + 1);
                table.appendChild(option);
            }

            day.appendChild(spanDays);
            day.appendChild(table);
            timetableDays.appendChild(day);
        }

        content.appendChild(timetableDays);
    }

    function createLessons(option,day) {

        let inputBlock = document.querySelector(`.${day.className} .inputBlock`);
        if (inputBlock) {
            inputBlock.children.length < option ? addBlocks(option,inputBlock,day) : removeBlocks(option,inputBlock);
        }
        else {
            inputBlock = document.createElement('div');
            inputBlock.className = 'inputBlock';
            fillingOut(option,inputBlock,day);
        }
    }

    function addBlocks(option,inputBlock,day) {
        for (let i = inputBlock.children.length; i < option; i++) {
            let input = document.createElement('input');
            input.style = 'width: 20%; margin: 20px';
            input.type = 'text';
            inputBlock.appendChild(input);
        }
        day.appendChild(inputBlock);
    }

    function removeBlocks(option,inputBlock) {
        for (let i = inputBlock.children.length; i > option; i--) {
            inputBlock.removeChild(inputBlock.lastChild);
        }
    }

    function fillingOut(option,inputBlock,day) {
        for (let i = 0; i < option; i++) {
            let input = document.createElement('input');
            input.style = 'width: 20%; margin: 20px';
            input.type = 'text';
            inputBlock.appendChild(input);
        }
        day.appendChild(inputBlock);
    }
    function submitTimeTable() {
        let button = document.createElement('button');
        button.className = 'submitBtn';
        let wrapper = document.querySelector('.timeTablesDays');
        button.innerHTML = 'submit';
        wrapper.appendChild(button);

    }
}

export let a = 'Antox Petyx';