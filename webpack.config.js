const path = require('path');
const HTMLWebpackConfig = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
    context: path.resolve(__dirname),
    mode: "development",
    entry: {
        main: './main.js',
        constructor: './constructor.js',
    },
    output: {
        filename: "[name].[contenthash].js",
        path: path.resolve(__dirname, 'dist'),
    },
    plugins: [
        new HTMLWebpackConfig({
            template: './index.html',
        }),
        new HTMLWebpackConfig({
            filename: "constructor.html",
            template: './constructor.html',
            chunks: false,
        }),
        new CleanWebpackPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ['style-loader','css-loader'],
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                use: ['file-loader']
            }
        ]
    }
};