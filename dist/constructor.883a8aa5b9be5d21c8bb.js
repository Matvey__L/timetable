/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./constructor.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./constructor.js":
/*!************************!*\
  !*** ./constructor.js ***!
  \************************/
/*! exports provided: a */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"a\", function() { return a; });\nlet url = document.URL;\r\nif (url.includes(\"constructor\")) {\r\n    let date = new Date();\r\n    let group = document.createElement(\"button\");\r\n    group.class = 'group';\r\n    group.innerHTML = '10703217';\r\n    group.addEventListener(\"click\", changeTimeTable);\r\n\r\n    let content = document.querySelector(\".content\");\r\n    content.appendChild(group);\r\n\r\n    function changeTimeTable() {\r\n        let daysArrRu = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];\r\n        let daysArrEng = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];\r\n        let timetableDays = document.createElement('div');\r\n        timetableDays.className = 'timeTablesDays';\r\n\r\n        for (let i in daysArrRu) {\r\n            let day =  document.createElement('div');\r\n            day.className = daysArrEng[i];\r\n            day.style.margin = \"30px\";\r\n            let spanDays = document.createElement('span');\r\n            spanDays.innerHTML = daysArrRu[i];\r\n            let table = document.createElement(\"select\");\r\n            table.onchange = function () {\r\n                createLessons(table.options[table.selectedIndex].value, day);\r\n                \r\n                if (!document.querySelector('.submitBtn')) submitTimeTable();\r\n            };\r\n\r\n            for (let i = 0; i < 5; i++) {\r\n                let option = document.createElement('option');\r\n                option.value = String(i + 1);\r\n                option.innerHTML = String(i + 1);\r\n                table.appendChild(option);\r\n            }\r\n\r\n            day.appendChild(spanDays);\r\n            day.appendChild(table);\r\n            timetableDays.appendChild(day);\r\n        }\r\n\r\n        content.appendChild(timetableDays);\r\n    }\r\n\r\n    function createLessons(option,day) {\r\n\r\n        let inputBlock = document.querySelector(`.${day.className} .inputBlock`);\r\n        if (inputBlock) {\r\n            inputBlock.children.length < option ? addBlocks(option,inputBlock,day) : removeBlocks(option,inputBlock);\r\n        }\r\n        else {\r\n            inputBlock = document.createElement('div');\r\n            inputBlock.className = 'inputBlock';\r\n            fillingOut(option,inputBlock,day);\r\n        }\r\n    }\r\n\r\n    function addBlocks(option,inputBlock,day) {\r\n        for (let i = inputBlock.children.length; i < option; i++) {\r\n            let input = document.createElement('input');\r\n            input.style = 'width: 20%; margin: 20px';\r\n            input.type = 'text';\r\n            inputBlock.appendChild(input);\r\n        }\r\n        day.appendChild(inputBlock);\r\n    }\r\n\r\n    function removeBlocks(option,inputBlock) {\r\n        for (let i = inputBlock.children.length; i > option; i--) {\r\n            inputBlock.removeChild(inputBlock.lastChild);\r\n        }\r\n    }\r\n\r\n    function fillingOut(option,inputBlock,day) {\r\n        for (let i = 0; i < option; i++) {\r\n            let input = document.createElement('input');\r\n            input.style = 'width: 20%; margin: 20px';\r\n            input.type = 'text';\r\n            inputBlock.appendChild(input);\r\n        }\r\n        day.appendChild(inputBlock);\r\n    }\r\n    function submitTimeTable() {\r\n        let button = document.createElement('button');\r\n        button.className = 'submitBtn';\r\n        let wrapper = document.querySelector('.timeTablesDays');\r\n        button.innerHTML = 'submit';\r\n        wrapper.appendChild(button);\r\n\r\n    }\r\n}\r\n\r\nlet a = 'Antox Petyx';\n\n//# sourceURL=webpack:///./constructor.js?");

/***/ })

/******/ });